# Progetto applicazione che visualizza i dati sulla posizione corrente del dispositivo mobile.

## Finalita'
L'applicazione si basa sull'utilizzo di activity e di intent espliciti

## Descrizione
L'app e' costituita da 2 activity. Nell'activity main deve essere presente un bottone che una volta premuto lancia una seconda activity nel quale viene stampata la posizione corrente.
L'activity main deve avere riportare un messaggio che specifica cosa fa l'applicazione.

Si riporta del codice per raggiunge l'obiettivo

```

      TextView txtPosizione = findViewById (R.id.txtPosizione);
      Strign msg = "Posizione non trovata";
      if (pos != null) {
          double lon = pos.getLongitude();
          double lat = pos.getLatitude();
          double acc = pos.getAccuracy();
          double alt = 0;
          txtPosizione.setText(msg);
          if (pos.hasAltitude()) {
              alt = pos.getAltitude();
              }
          msg = "Longitudine = " + lon + "\n";
          msg += "Latitudine = " + lat + "\n";
          msg += "Precisione = " + acc + "\n";
          msg += "Altitudine = " + alt + "\n";
      }
      txtPosizione.setText(msg);


 ...
  String servizio = Context.LOCATION_SERVICE;
  locationManager = (LocationManager)getSystemService(servizio);
  Location posizione = locationManager.getLastKnownLocation(sistema);
...

```

Per collaudare l'applicazione con un emulatore e' necessario simulare una posizione GPS inserendo 
manualmente i valori di longitudine e latitudine attraverso l'android console.
Dal prompt dei comandi digitare telnet localhost 5554. Il comando da usare dopo aver avviato l'emulatore e':
geo fix longitudine latitudine
